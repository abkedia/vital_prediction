# Vehicle vitals

This repo contains for vehicle vitals prediction. It consits of two main notebooks - 

1. `model_generation.ipynb` - contains code for generating model for predicting if a vehicle is in degraded state.
2. `report_generator.ipynb` - contains code that runs in production and fires every 3 hrs and emails the report generated using model prediction and some business rules.

